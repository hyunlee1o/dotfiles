;;https://github.com/bbatsov/emacs.d/blob/master/init.el
;; (setq package-enable-at-startup nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;; '(custom-safe-themes
 ;;   '("d2e0c53dbc47b35815315fae5f352afd2c56fa8e69752090990563200daae434" default))
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa" . "https://melpa.org/packages/")
     ("melpa-stable" . "https://stable.melpa.org/packages/")
     ("org" . "http://orgmode.org/elpa/")))
 
 ;; '(package-selected-packages
 )


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (
                ;; :inherit nil
                ;; :stipple nil
                ;; :background "#000000"
                ;; :foreground "#a3ecd9"
                ;; :inverse-video nil
                ;; :box nil
                ;; :strike-through nil
                ;; :overline nil
                ;; :underline nil
                :slant normal
                :weight normal :height 140
                :width normal
                :family "Hack"))))
 )

;;Package configuration
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                  (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  ;; (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;; (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  ;; (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
  (package-initialize)
  )

;;native-comp system to automatically generate the natively compiled files when Emacs loads a new .elc file
;;deferred/async compilation
(defconst dd/using-native-comp-p (fboundp 'native-comp-available-p))
(when dd/using-native-comp-p
  (setq native-comp-async-query-on-exit t)
  (setq native-comp-async-jobs-number 4)
  (setq native-comp-async-report-warnings-errors nil)
  (setq native-comp-deferred-compilation t)
  (setq native-comp-deferred-compilation-deny-list ()) ;;HACK TO BE REMOVED
  )

(setq use-package-always-ensure t
      use-package-always-defer t
      ;; straight-use-package-by-default t
      )

(eval-when-compile
  (require 'use-package))
(use-package use-package-ensure-system-package)

;;If no packages refresh
(when (not package-archive-contents)
  (package-refresh-contents))

;;extend config in multiple folders
;; Directory where files for emacs are located
(setq user-emacs-directory (expand-file-name "~/.emacs.d/"))
;; elisp load-path
;; (add-to-list 'load-path (concat user-emacs-directory "elisp/"))
;; (add-to-list 'load-path (concat user-emacs-directory "config/"))
;; (add-to-list 'load-path (concat user-emacs-directory "config/packages/"))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;Use straight in use-package
(straight-use-package 'use-package)

(use-package diminish
  :straight t
  )

(use-package bind-key
  :straight t
  )

;;General configuration
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This section is for overriding common emacs keybindings with tweaks.
(global-unset-key (kbd "C-z")) ;; I hate you so much C-z
(global-set-key (kbd "C-z") 'undo) ;; I hate you so much C-z
;; (global-set-key (kbd "M-<right>") 'indent-rigidly-right)
;; (global-set-key (kbd "M-<left>") 'indent-rigidly-left)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This section is for global settings for built-in emacs parameters
(setq
 inhibit-startup-screen t
 inhibit-startup-message t
 initial-scratch-message nil
 inhibit-splash-screen nil
 enable-local-variables t
 create-lockfiles nil
 make-backup-files nil
 custom-file (expand-file-name "custom.el" user-emacs-directory)
 column-number-mode t
 scroll-error-top-bottom t
 scroll-margin 3
 gc-cons-threshold 100000000
 large-file-warning-threshold 100000000
 user-full-name "Jorge Sanchez"
 auto-package-update-delete-old-versions t ;;update packages delete
 auto-package-update-hide-results t ;;update packages without bothering
 indicate-empty-lines t
 visible-bell t)

(setq
 backup-by-copying t ; don't clobber symlinks
 backup-directory-alist '(("." . "~/.saves")) ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t
 )


(setq
 flycheck-checker-error-threshold nil
 confirm-kill-emacs 'yes-or-no-p
 toggle-truncate-lines 1
 display-time-24hr-format t
 )

(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)
(display-time-mode 1)
(global-prettify-symbols-mode t) ;;https://www.modernemacs.com/post/prettify-mode/

(put 'dired-find-alternate-file 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; buffer local variables
(setq-default
 fill-column 80
 indent-tabs-mode t
 default-tab-width 4
 tab-always-indent 'complete
 )

(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . dark))

(let ((display-table (or standard-display-table (make-display-table))))
  (set-display-table-slot display-table 'vertical-border (make-glyph-code ?│)) ; or ┃ │
  (setq standard-display-table display-table))
(set-face-background 'vertical-border (face-background 'default))
(set-face-foreground 'vertical-border "grey")

;; mode to restrict buffer columns
(use-package olivetti
  :straight t
  )

;; speedbar
(use-package speedbar
  :straight t
  :config
  (when window-system          ; start speedbar if we're using a window system
    (speedbar t)
    (setq speedbar-mode-hook '(lambda ()
				(interactive)
				(other-frame 0)))))

;; (use-package zoom-frm
;;   :straight (zoom-frm :type git
;;                       :host github
;;                       :repo "emacsmirror/emacswiki.org"
;;                       :files ("zoom-frm.el"))
;;   :config
;;   (dotimes (i my/default-zoom-level) (zoom-frm-in)))

;; make your buffers partition themselves
(use-package zoom
  :straight t
  :hook (after-init . zoom-mode)
  :config (setq zoom-size '(0.618 . 0.618))
  (setq zoom-ignore-predicates (quote ((lambda nil (window-minibuffer-p)))))
  (setq zoom-ignore-predicates '((lambda () (< (count-lines (point-min) (point-max)) 20))))
  (setq zoom-ignored-major-modes '(helm-major-mode ediff-mode lsp-treemacs-symbols lsp-mode))
  ;; (setq zoom-ignored-buffer-name-regexps '(" *Minibuf-1*" "^*helm" "*helm org inbuffer*" "*Org Help*" "*Diff*" "^*Ediff" "^*Ediff Control Panel*" "^*Ediff Registr*y" "^\*LSP.*"))
  ;; Helm
  (add-to-list 'zoom-ignored-buffer-names "*LSP Symbols List*")
  (add-to-list 'zoom-ignored-buffer-name-regexps " *Minibuf-1*")
  (add-to-list 'zoom-ignored-buffer-name-regexps "^*helm")
  (add-to-list 'zoom-ignored-buffer-name-regexps "*helm org inbuffer*")
  (add-to-list 'zoom-ignored-buffer-name-regexps "*Org Help*")
  (add-to-list 'zoom-ignored-buffer-name-regexps "^*.pdf")
  ;; ;; FIXME
  ;; ;; Ediff
  (add-to-list 'zoom-ignored-buffer-name-regexps "*Diff*")
  (add-to-list 'zoom-ignored-buffer-name-regexps "^*Ediff")
  (add-to-list 'zoom-ignored-buffer-name-regexps "^*Ediff Control Panel*")
  (add-to-list 'zoom-ignored-buffer-name-regexps "^*Ediff Registry*")
  (add-to-list 'zoom-ignored-buffer-name-regexps "*LSP Symbols List*")
  )

;; execute current buffer
(use-package quickrun
  :straight t
  :bind ("C-c r" . quickrun))

;;Save hist
(savehist-mode 1)
(setq savehist-file "~/.emacs.d/.savehist")
(setq history-length 25
      history-delete-duplicates t
      savehist-save-minibuffer-history 1
      savehist-additional-variables
      '(kill-ring
        search-ring
        regexp-search-ring))

;;Save place in a buffer mode
(save-place-mode 1)

;; edit buffers are brighter
(use-package solaire-mode
  :straight t
  :init
  (solaire-global-mode 1)
  ;; :config
  ;; (add-to-list 'solaire-mode-themes-to-face-swap "^doom-")
  )

;; my theme
(use-package doom-themes
  :straight t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-one t)
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)
  
  ;;tabs configuration
  (global-tab-line-mode 1)
  (set-face-attribute 'tab-line nil ;; background behind tabs
                      :background "gray40"
                      :foreground "gray60" :distant-foreground "gray50"
                      :family "Hack" :height 1.0 :box nil)
  (set-face-attribute 'tab-line-tab nil ;; active tab in another window
                      :inherit 'tab-line
                      :foreground "gray70" :background "gray90" :box nil)
  (set-face-attribute 'tab-line-tab-current nil ;; active tab in current window
                      :background "black" :foreground "white" :box nil)
  ;; :background "white" :foreground "white" :box nil)
  (set-face-attribute 'tab-line-tab-inactive nil ;; inactive tab
                      :background "gray80" :foreground "black" :box nil)
  (set-face-attribute 'tab-line-highlight nil ;; mouseover
                      :background "orange" :foreground "black")
  :defer 0.5)


(use-package doom-modeline
  :straight t
  :init (doom-modeline-mode 1)
  :config (setq doom-modeline-project-detection 'project)
  )

;; (use-package darktooth-theme
;;   :straight t
;;   :init (load-theme 'darktooth t))

(use-package midnight
  :straight t
  :config (midnight-delay-set 'midnight-delay "12:00am")
  (setq midnight-period 7200)
  :hook (midnight . clean-buffer-list)
  )

(use-package wakatime-mode
  :straight t
  :ensure-system-package wakatime
  :config
  (setq wakatime-api-key "31b3251c-bb8e-4de0-878e-3d3fc1aacf93")
  :init
  (global-wakatime-mode)
  :diminish wakatime-mode
  )

(use-package tramp
  :config
  (setq tramp-default-method "ssh" tramp-verbose 1
        tramp-histfile-override t)
  (setf tramp-persistency-file-name
        (concat temporary-file-directory "tramp-" (user-login-name))))

(defun efs/configure-eshell ()
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)

  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)

  ;; Bind some useful keys for evil-mode
  ;; (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'counsel-esh-history)
  ;; (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  ;; (evil-normalize-keymaps)

  (setq eshell-history-size         10000
        eshell-buffer-maximum-lines 10000
        eshell-hist-ignoredups t
        eshell-scroll-to-bottom-on-input t))

(use-package eshell-git-prompt :straight t)

(use-package eshell
  :straight t
  :hook (eshell-first-time-mode . efs/configure-eshell)
  :bind ([f1] . eshell)
  :init
  (setf eshell-directory-name (locate-user-emacs-file "local/eshell"))
  :config
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "vim")))
  (add-hook 'eshell-mode-hook ; Bad, eshell, bad!
            (lambda ()
              (define-key eshell-mode-map (kbd "<f1>") #'quit-window)))
  (eshell-git-prompt-use-theme 'powerline)
  )

;; ;;symbol-overlay
;; ;; a highlight-symbol replacement.
;; (use-package symbol-overlay
;;   :straight t
;;   :hook (prog-mode . symbol-overlay)
;;   ;; :config
;;   ;; (setq symbol-overlay-map (make-sparse-keymap))
;;   ;; (setq my/symbol-overlay-keymap (make-sparse-keymap))
;;   ;; (define-key my/symbol-overlay-keymap (kbd "h") 'symbol-overlay-put)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "n") 'symbol-overlay-jump-next)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "p") 'symbol-overlay-jump-prev)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "w") 'symbol-overlay-save-symbol)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "t") 'symbol-overlay-toggle-in-scope)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "e") 'symbol-overlay-echo-mark)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "d") 'symbol-overlay-jump-to-definition)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "s") 'symbol-overlay-isearch-literally)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "q") 'symbol-overlay-query-replace)
;;   ;; (define-key my/symbol-overlay-keymap (kbd "r") 'symbol-overlay-rename)
;;   ;; :bind ("C-c s" . symbol-overlay-map)
;;   ;; :diminish symbol-overlay
;;   )

(use-package unicode-fonts
  :straight t
  :config
  (unicode-fonts-setup))

(use-package pretty-mode
  :straight t
  :hook (prog-mode . pretty-mode)
  :config
  (global-pretty-mode t))

(use-package ligature
  ;; :straight t
  :straight (:host github :repo "mickeynp/ligature.el")
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

;; (use-package composite
;;   :straight t
;;   :hook (prog-mode . auto-composition-mode)
;;   :init (global-auto-composition-mode -1))

(use-package paren
  :config
  (show-paren-mode +1))

(use-package subword
  :ensure nil
  :diminish subword-mode
  :config (global-subword-mode 1))

;; GUI bars
;; (tooltip-mode -1)
(tool-bar-mode -1)
;; (menu-bar-mode -1)
;; (scroll-bar-mode -1)
(global-display-line-numbers-mode)
(delete-selection-mode 1)
;;stop trashing
(setq make-backup-files nil) ;; Hello GIT

;;(set-face-attribute 'whitespace-line nil
;; :foreground nil
;; :background "#555"
;; :weight 'bold)
;;   (setq tab-line-new-button-show nil
;;         ;; tab-line-close-button-show nil
;;         tab-line-exclude-modes '(cider-test-report-mode
;;                                  deft-mode
;;                                  magit-mode
;;                                  magit-status-mode
;;                                  magit-diff-mode
;;                                  magit-log-mode
;;                                  magit-process-mode
;;                                  magit-popup-mode
;;                                  term-mode
;;                                  text-mode
;;                                  tide-references-mode
;;                                  xref--xref-buffer-mode))
;;   (set-face-attribute 'tab-line nil ;; background behind tabs
;;                       :background "#eee8d5" ; base2
;;                       :foreground "#657b83" ; base00
;;                       :distant-foreground "#586e75" ; base01
;;                       :family "Menlo"
;;                       :height 0.95
;;                       :box nil)
;;   (set-face-attribute 'tab-line-tab nil ;; active tab in another window
;;                       :inherit 'tab-line
;;                       :background "#fdf6e3" ; base3
;;                       :foreground "#586e75" ; base01
;;                       :box nil)
;;   (set-face-attribute 'tab-line-tab-current nil ;; active tab in current window
;;                       :background "#fdf6e3" ; base3
;;                       :foreground "#586e75" ; base01
;;                       :box nil)
;;   (set-face-attribute 'tab-line-tab-inactive nil ;; inactive tab
;;                       :background "#eee8d5" ; base2
;;                       :foreground "#93a1a1" ; base1
;;                       :box nil)
;;   (set-face-attribute 'tab-line-highlight nil ;; mouseover
;;                       :background "#eee8d5" ; base2
;;                       :foreground 'unspecified)
;;   ;; keyboard shortcuts
;; (global-set-key (kbd "S-+") #'tab-line-switch-to-prev-tab)
;; (global-set-key (kbd "S-ç") #'tab-line-switch-to-next-tab)

;;indent mode
(use-package highlight-indentation
  :straight t
  :hook (prog-mode . highlight-indentation-current-column-mode)
  (prog-mode . highlight-indentation-mode)
  :config
  (highlight-indentation-current-column-mode 1)
  :diminish highlight-indentation-current-column-mode
  )


;; protects against accidental mouse movements
;; http://stackoverflow.com/a/3024055/1041691
(add-hook 'mouse-leave-buffer-hook
          (lambda () (when (and (>= (recursion-depth) 1)
			 (active-minibuffer-window))
                  (abort-recursive-edit))))

;; *scratch* is immortal
(add-hook 'kill-buffer-query-functions
          (lambda () (not (member (buffer-name) '("*scratch*" "scratch.el")))))


;; Intellij-like text moving
(use-package move-text
  :straight t
  :bind
  (("M-<up>" . move-text-up)
   ("M-<down>" . move-text-down))
  )

;;recentf
(use-package recentf
  :bind
  ("C-x C-r" . recentf-open-files)
  :config
  (recentf-mode)
  (setq  recentf-max-saved-items 50
	 recentf-max-menu-items 15
         recentf-exclude '("^/var/folders\\.*"
                           "COMMIT_EDITMSG\\'"
                           ".*-autoloads\\.el\\'"
                           "[/\\]\\.elpa/")
	 recentf-auto-cleanup 'never
	 )
  )

;; (use-package find-file-in-project)

;;Auto-complete for emacs
(use-package auto-complete
  :straight t
  :config (ac-config-default)
  :diminish auto-complete-mode)

(use-package which-key
  :straight t
  :init
  (which-key-mode 1)
  :diminish 'which-key-mode)


;;highlight uncommited changes
(use-package diff-hl
  :straight t
  :config
  (global-diff-hl-mode +1)
  (global-diff-hl-amend-mode)
  :hook (prog-mode . diff-hl-mode)
  (dired-mode . diff-hl-dired-mode)
  (magit-post-refresh . diff-hl-magit-post-refresh)
  :diminish diff-hl-amend-mode
  )

;; highlight the current line
(use-package hl-line
  :straight t
  :config
  (global-hl-line-mode +1))

(use-package whitespace-cleanup-mode
  :straight t
  ;; :config
  ;; (global-whitespace-cleanup-mode 1)
  :hook
  (lsp-mode . whitespace-cleanup-mode)
  (python-mode . whitespace-cleanup-mode)
  (prog-mode . whitespace-cleanup-mode)
  :diminish whitespace-cleanup-mode
  )


(use-package undo-tree
  :straight t
  :config
  ;; autosave the undo-tree history
  (setq undo-tree-history-directory-alist
        `((".*" . ,temporary-file-directory)))
  (setq undo-tree-auto-save-history t)
  (global-undo-tree-mode +1)
  :diminish 'undo-tree-mode)

;; (use-package crux

;;   :bind (("C-c o" . crux-open-with)
;;          ("M-o" . crux-smart-open-line)
;;          ("C-c n" . crux-cleanup-buffer-or-region)
;;          ("C-c f" . crux-recentf-find-file)
;;          ("C-M-z" . crux-indent-defun)
;;          ("C-c u" . crux-view-url)
;;          ("C-c e" . crux-eval-and-replace)
;;          ("C-c w" . crux-swap-windows)
;;          ("C-c D" . crux-delete-file-and-buffer)
;;          ("C-c r" . crux-rename-buffer-and-file)
;;          ("C-c t" . crux-visit-term-buffer)
;;          ("C-c k" . crux-kill-other-buffers)
;;          ("C-c TAB" . crux-indent-rigidly-and-copy-to-clipboard)
;;          ("C-c I" . crux-find-user-init-file)
;;          ("C-c S" . crux-find-shell-init-file)
;;          ("s-r" . crux-recentf-find-file)
;;          ("s-j" . crux-top-join-line)
;;          ("C-^" . crux-top-join-line)
;;          ("s-k" . crux-kill-whole-line)
;;          ("C-<backspace>" . crux-kill-line-backwards)
;;          ("s-o" . crux-smart-open-line-above)
;;          ([remap move-beginning-of-line] . crux-move-beginning-of-line)
;;          ([(shift return)] . crux-smart-open-line)
;;          ([(control shift return)] . crux-smart-open-line-above)
;;          ([remap kill-whole-line] . crux-kill-whole-line)
;;          ("C-c s" . crux-ispell-word-then-abbrev)))


;;Interesting hydras https://github.com/abo-abo/hydra/wiki/
(use-package hydra
  :straight t
  )

(use-package multiple-cursors
  :functions hydra-multiple-cursors
  :bind
  ("C-c m" . hydra-multiple-cursors/body)
  :preface
  ;; insert specific serial number
  (defvar ladicle/mc/insert-numbers-hist nil)
  (defvar ladicle/mc/insert-numbers-inc 1)
  (defvar ladicle/mc/insert-numbers-pad "%01d")

  (defun ladicle/mc/insert-numbers (start inc pad)
    "Insert increasing numbers for each cursor specifically."
    (interactive
     (list (read-number "Start from: " 0)
           (read-number "Increment by: " 1)
           (read-string "Padding (%01d): " nil ladicle/mc/insert-numbers-hist "%01d")))
    (setq mc--insert-numbers-number start)
    (setq ladicle/mc/insert-numbers-inc inc)
    (setq ladicle/mc/insert-numbers-pad pad)
    (mc/for-each-cursor-ordered
     (mc/execute-command-for-fake-cursor
      'ladicle/mc--insert-number-and-increase
      cursor)))

  (defun ladicle/mc--insert-number-and-increase ()
    (interactive)
    (insert (format ladicle/mc/insert-numbers-pad mc--insert-numbers-number))
    (setq mc--insert-numbers-number (+ mc--insert-numbers-number ladicle/mc/insert-numbers-inc)))
  :config
  (with-eval-after-load 'hydra
    (defhydra hydra-multiple-cursors (:color pink :hint nil)
      "
                                                                        ╔════════╗
    Point^^^^^^             Misc^^            Insert                            ║ Cursor ║
  ──────────────────────────────────────────────────────────────────────╨────────╜
     _k_    _K_    _M-k_    [_l_] edit lines  [_i_] 0...
     ^↑^    ^↑^     ^↑^     [_m_] mark all    [_a_] letters
    mark^^ skip^^^ un-mk^   [_s_] sort        [_n_] numbers
     ^↓^    ^↓^     ^↓^
     _j_    _J_    _M-j_
  ╭──────────────────────────────────────────────────────────────────────────────╯
                           [_q_]: quit, [Click]: point
"
      ("l" mc/edit-lines :exit t)
      ("m" mc/mark-all-like-this :exit t)
      ("j" mc/mark-next-like-this)
      ("J" mc/skip-to-next-like-this)
      ("M-j" mc/unmark-next-like-this)
      ("k" mc/mark-previous-like-this)
      ("K" mc/skip-to-previous-like-this)
      ("M-k" mc/unmark-previous-like-this)
      ("s" mc/mark-all-in-region-regexp :exit t)
      ("i" mc/insert-numbers :exit t)
      ("a" mc/insert-letters :exit t)
      ("n" ladicle/mc/insert-numbers :exit t)
      ("<mouse-1>" mc/add-cursor-on-click)
      ;; Help with click recognition in this hydra
      ("<down-mouse-1>" ignore)
      ("<drag-mouse-1>" ignore)
      ("q" nil)))
  )

(use-package ace-window
  :straight t
  :bind ("M-o" . hydra-window/body)
  :config
  (setq aw-dispatch-always t)
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  (defhydra hydra-window (:color blue)
    "window"
    ("h" windmove-left "left")
    ("j" windmove-down "down")
    ("k" windmove-up "up")
    ("l" windmove-right "right")
    ("a" ace-window "ace")
    ("s" (lambda () (interactive) (ace-window 4)) "swap")
    ("d" (lambda () (interactive) (ace-window 16)) "delete")
    ("q" nil "Quit"))
  );;   :init
;;   ;; In case commands behavior is messy with multiple-cursors,
;;   ;; check your ~/.emacs.d/.mc-lists.el
;;   (defun mc/check-command-behavior ()
;;     "Open ~/.emacs.d/.mc-lists.el.
;;   So you can fix the list for run-once and run-for-all multiple-cursors commands."
;;     (interactive)
;;     (find-file "~/.emacs.d/.mc-lists.el"))
;;   :config
;;   (defhydra hydra-multiple-cursors (:columns 3 :idle 1.0)
;;     "Multiple cursors"
;;     ("l" mc/edit-lines "Edit lines in region" :exit t)
;;     ("b" mc/edit-beginnings-of-lines "Edit beginnings of lines in region" :exit t)
;;     ("e" mc/edit-ends-of-lines "Edit ends of lines in region" :exit t)
;;     ("a" mc/mark-all-like-this "Mark all like this" :exit t)
;;     ("S" mc/mark-all-symbols-like-this "Mark all symbols likes this" :exit t)
;;     ("w" mc/mark-all-words-like-this "Mark all words like this" :exit t)
;;     ("r" mc/mark-all-in-region "Mark all in region" :exit t)
;;     ("R" mc/mark-all-in-region-regexp "Mark all in region (regexp)" :exit t)
;;     ("i" (lambda (n)
;;            (interactive "nInsert initial number: ")
;;            (mc/insert-numbers n))
;;      "Insert numbers")
;;     ("s" mc/sort-regions "Sort regions")
;;     ("v" mc/reverse-regions "Reverse order")
;;     ("d" mc/mark-all-dwim "Mark all dwim")
;;     ("n" mc/mark-next-like-this "Mark next like this")
;;     ("N" mc/skip-to-next-like-this "Skip to next like this")
;;     ("M-n" mc/unmark-next-like-this "Unmark next like this")
;;     ("p" mc/mark-previous-like-this "Mark previous like this")
;;     ("P" mc/skip-to-previous-like-this "Skip to previous like this")
;;     ("M-p" mc/unmark-previous-like-this "Unmark previous like this")
;;     ("q" nil "Quit" :exit t))
;;   :bind
;;   ("C-c m" . hydra-multiple-cursors/body))

(use-package ace-window
  :straight t
  :bind ("M-o" . hydra-window/body)
  :config
  (setq aw-dispatch-always t)
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  (defhydra hydra-window (:color blue)
    "window"
    ("h" windmove-left "left")
    ("j" windmove-down "down")
    ("k" windmove-up "up")
    ("l" windmove-right "right")
    ("a" ace-window "ace")
    ("s" (lambda () (interactive) (ace-window 4)) "swap")
    ("d" (lambda () (interactive) (ace-window 16)) "delete")
    ("q" nil "Quit"))
  )

(use-package dumb-jump
  :straight t
  :bind
  ("C-c j" . hydra-dumb-jump/body)
  :config
  (setq dumb-jump-selector 'ivy)
  (defhydra hydra-dumb-jump (:color blue)
    "Dumb Jump"
    ("g" dumb-jump-go "Jump to def")
    ("p" dumb-jump-back "Jump back")
    ("q" dumb-jump-quick-look "Quick look")
    ("o" dumb-jump-go-other-window "Jump in other window")
    ("q" nil "Quit")))

;;sudo stuff
(use-package sudo-edit
  :straight t
  )

;; (use-package dired-toggle-sudo
;;   :straight t
;;   :init
;;   (define-key dired-mode-map (kbd "C-c C-s") 'dired-toggle-sudo)
;;   (eval-after-load 'tramp
;;     '(progn
;;        ;; Allow to use: /sudo:user@host:/path/to/file
;;        (add-to-list 'tramp-default-proxies-alist
;; 		    '(".*" "\\`.+\\'" "/ssh:%h:")))))

;;autosave replacement
(use-package super-save
  :straight t
  :config
  (add-to-list 'super-save-triggers 'ace-window)
  (super-save-mode +1)
  :diminish 'super-save-mode
  )

(use-package all-the-icons
  :straight t
  ;; :config (all-the-icons-install-fonts)
  )

(use-package all-the-icons-dired
  :straight t
  :hook
  (dired-mode . all-the-icons-dired-mode))

(use-package dirvish
  :straight t)

(use-package dired
  :ensure nil
  :straight nil
  :defer 1
  :commands (dired dired-jump)
  ;; :bind (:map dired-mode-map
  ;;             ("C-c C-n" . flycheck-tip-cycle))
  :config
  (setq dired-listing-switches "-agho --group-directories-first"
        dired-omit-files "^\\.[^.].*"
        dired-omit-verbose nil
        dired-hide-details-hide-symlink-targets nil
        delete-by-moving-to-trash t)

  (autoload 'dired-omit-mode "dired-x")

  (add-hook 'dired-load-hook
            (lambda ()
              (interactive)
              (dired-collapse)))

  (add-hook 'dired-mode-hook
            (lambda ()
              (interactive)
              ;; (dired-omit-mode 1)
              ;; (dired-hide-details-mode 1)
              ;; (unless (or dw/is-termux
              ;;             (s-equals? "/gnu/store/" (expand-file-name default-directory)))
              (all-the-icons-dired-mode 1))
            (hl-line-mode 1))
  )


(use-package dired-rainbow
  :defer 2
  :config
  (dired-rainbow-define-chmod directory "#6cb2eb" "d.*")
  (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
  (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
  (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
  (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
  (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
  (dired-rainbow-define media "#de751f" ("mp3" "mp4" "mkv" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
  (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg"))
  (dired-rainbow-define log "#c17d11" ("log"))
  (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
  (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
  (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
  (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
  (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
  (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
  (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
  (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
  (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
  (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
  (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*"))

(use-package dired-single
  :straight t
  :defer t)

(use-package dired-ranger
  :straight t
  :defer t)

(use-package dired-collapse
  :straight t
  :defer t)

(use-package dired-open
  :straight t
  :config
  ;; Doesn't work as expected!
  ;;(add-to-list 'dired-open-functions #'dired-open-xdg t)
  (setq dired-open-extensions '(("png" . "feh")
                                ("mkv" . "mpv"))))

;; (use-package dired-hide-dotfiles
;;   :hook (dired-mode . dired-hide-dotfiles-mode)
;; )


;;Make focussed & file visiting buffers stand out
(use-package dimmer
  :straight t
  :hook (after-init . dimmer-mode)
  :config
  (setq dimmer-fraction 0.2)
  (dimmer-configure-hydra)
  (dimmer-configure-magit)
  (dimmer-configure-org)
  (dimmer-configure-posframe)
  (add-to-list 'dimmer-buffer-exclusion-regexps "^magit.*")
  :diminish dimmmer)

;; (use-package neotree
;;   :straight t
;;   :bind ("<f8>" . 'neotree-toggle)
;;   :init
;;   ;; slow rendering
;;   (setq inhibit-compacting-font-caches t)
;;   ;; set icons theme
;;   (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
;;   ;; Every time when the neotree window is opened, let it find current file and jump to node
;;   (setq neo-smart-open t)
;;   ;; When running ‘projectile-switch-project’ (C-c p p), ‘neotree’ will change root automatically
;;   (setq projectile-switch-project-action 'neotree-projectile-action)
;;   ;; show hidden files
;;   (setq-default neo-show-hidden-files t))

(use-package projectile
  :straight t
  :init
  (projectile-mode +1)
  :bind (:map projectile-mode-map
              ;; ("s-p" . projectile-command-map)
              ("C-c p" . projectile-command-map))
  :config (projectile-global-mode)
  )

;; dependency of py-pygment
(use-package buftra
  :straight (:host github :repo "humitos/buftra.el")
  :after python)

;; pip install py-isort
(use-package py-isort
  :straight t)

;; formats docstrings ;; pip isntall docformatter
(use-package py-docformatter
  :straight (:host github :repo "humitos/py-cmd-buffer.el")
  ;; :hook (python-mode . py-docformatter-enable-on-save)
  :config
  (setq py-docformatter-options '("--wrap-summaries=80" "--pre-summary-newline")))

;; unify the " and ' ;; pip install unify
(use-package py-unify
  :straight (:host github :repo "humitos/py-cmd-buffer.el")
  :hook (python-mode . py-unify-enable-on-save)
  )

;; generate python docstrings ;; pip install pyment
(use-package py-pyment
  :straight (:host github :repo "humitos/py-cmd-buffer.el")
  :after python)

;; python docstring highlighting
(use-package python-docstring
  :straight t
  :after python)

;; python
;; sudo apt install virtualenv python-venv
;; sudo pip install 'python-lsp-server[all]'
;; https://github.com/python-lsp/python-lsp-server
;; (use-package elpy
;;   :straight t
;;   :mode "\\.hs\\'"
;;   :bind
;;   (:map elpy-mode-map
;;         ("C-M-n" . elpy-nav-forward-block)
;;         ("C-M-p" . elpy-nav-backward-block))
;;   :hook ((elpy-mode . flymake-mode) ;; enable checks
;; 	 (elpy-mode . display-fill-column-indicator-mode) ;; Enable display of ruler for 80 chars
;; 	 (elpy-mode . direnv-mode)
;;          (elpy-mode . (lambda ()
;;                         (set (make-local-variable 'company-backends)
;;                              '((elpy-company-backend :with company-yasnippet)))))
;;          ;; (before-save . py-isort-before-save) ;; enable py-isort
;;          )
;;   :init
;;   (advice-add 'python-mode :before 'elpy-enable)
;;   :config
;;   (setq tab-width     4
;;         python-indent 4
;; 	python-shell-interpreter "ipython"
;; 	python-shell-interpreter-args "-i --simple-prompt"
;;         elpy-modules (delq 'elpy-module-flymake elpy-modules)
;;         elpy-shell-echo-output nil
;;         elpy-rpc-python-command "python3"
;; 	elpy-rpc-virtualenv-path 'current
;;         elpy-rpc-timeout 2
;; 	lsp-pyls-plugins-flake8-enabled t
;; 	)
;;   )

(use-package python
  :ensure nil
  :config
  (setq tab-width     4
	python-indent 4
	python-shell-interpreter "ipython"
	python-shell-interpreter-args "-i --simple-prompt"
	;; lsp-pyls-plugins-flake8-enabled t
	)
  :config
  (defun flymake-pylint-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list "epylint" (list local-file))))
  ;; (add-to-list 'flymake-allowed-file-name-masks
  ;;              '("\\.py\\'" flymake-pylint-init))
  (setq-default flymake-no-changes-timeout '3)
  (defun show-fly-err-at-point ()
    "If the cursor is sitting on a flymake error, display the message in the minibuffer"
    (require 'cl)
    (interactive)
    (let ((line-no (line-number-at-pos)))
      (dolist (elem flymake-err-info)
	(if (eq (car elem) line-no)
	    (let ((err (car (second elem))))
              (message "%s" (flymake-ler-text err)))))))
  :hook 
  (python-mode . flymake-mode) ;; enable checks
  (python-mode . display-fill-column-indicator-mode) ;; Enable display of ruler for 80 chars
  (python-mode . direnv-mode)
  (python-mode . company-mode)
  (post-command-hook . show-fly-err-at-point)
  ;; (before-save . py-isort-before-save) ;; enable py-isort
  :bind (
	 :map python-mode-map
	 ("M-<right>" . python-indent-shift-right)
	 ("M-<left>" . python-indent-shift-left)
	 )
  )

(use-package py-autoflake
  :straight (:host github :repo "humitos/py-autoflake.el")
  :hook (python-mode . py-autoflake-enable-on-save)
  :config
  (setq py-autoflake-options '("--remove-all-unused-imports" "--remove-unused-variables" "--ignore-init-module-imports" "--expand-star-imports"))
  :after python 
  )

(use-package python-black
  :straight t
  :after python)

(use-package py-autopep8
  :straight t
  ;; :hook (python-mode . py-autopep8-enable-on-save)
  :config
  (setq py-autopep8-options '("--max-line-length=80"))
  )

(use-package pylint
  :straight t
  ;; :hook ((python-mode . pylint-add-menu-items)
  ;;        (python-mode . pylint-add-key-bindings))
  :diminish pylint
  :defer t)

(use-package poetry
  :straight t)

;; (use-package lsp-sonarlint
;;   :straight t)

;; (use-package lsp-sonarlint-python
;;   :ensure nil
;;   :config
;;   (setq lsp-sonarlint-python-enabled nil)
;;   )


(use-package flymake
  :preface
  (defun flymake-setup-next-error-function ()
    (setq next-error-function 'flymake-next-error-compat))

  (defun flymake-next-error-compat (&optional n _)
    (flymake-goto-next-error n))

  (defun flymake-diagnostics-next-error ()
    (interactive)
    (forward-line)
    (when (eobp) (forward-line -1))
    (flymake-show-diagnostic (point)))

  (defun flymake-diagnostics-prev-error ()
    (interactive)
    (forward-line -1)
    (flymake-show-diagnostic (point)))
  :hook
  (flymake-mode-hook . flymake-setup-next-error-function)
  ;; :general
  ;; (:keymaps
  ;;  'flymake-mode-map
  ;;  :prefix
  ;;  local-leader-key
  ;;  "!" 'flymake-show-buffer-diagnostics)
  ;; (:keymaps
  ;;  'flymake-mode-map
  ;;  :prefix next-prefix
  ;;  "E" 'flymake-goto-next-error)
  ;; (:keymaps
  ;;  'flymake-mode-map
  ;;  :prefix prev-prefix
  ;;  "E" 'flymake-goto-prev-error)
  ;; (:keymaps
  ;;  'flymake-diagnostics-buffer-mode-map
  ;;  "n" 'flymake-diagnostics-next-error
  ;;  "p" 'flymake-diagnostics-prev-error
  ;;  "j" 'flymake-diagnostics-next-error
  ;;  "k" 'flymake-diagnostics-prev-error
  ;;  "RET" 'flymake-goto-diagnostic
  ;;  "TAB" 'flymake-show-diagnostic)
  :init
  (setq flymake-proc-ignored-file-name-regexps '("\\.l?hs\\'"))

  (remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake))

(use-package flymake-diagnostic-at-point
  :ensure t
  :preface
  (defun flymake-diagnostic-at-point-quick-peek (text)
    "Display the flymake diagnostic TEXT with `quick-peek'`."
    (quick-peek-show (concat flymake-diagnostic-at-point-error-prefix text)))
  :hook
  (flymake-mode-hook . flymake-diagnostic-at-point-mode)
  :init
  (setq flymake-diagnostic-at-point-error-prefix nil))

;;company
(use-package company
  :straight t
  :diminish company-mode
  :after lsp-mode
  :hook
  (prog-mode . company-mode)
  :init
  (global-company-mode)
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0)
  :config
  (setq company-idle-delay 0
	company-minimum-prefix-length 1
	company-tooltip-align-annotations t
	company-backends
	'((company-files          ; files & directory
	   company-keywords       ; keywords
	   company-capf           ; completion at point
	   company-yasnippet      ;snippets
	   )  ; completion-at-point-functions
	  (company-abbrev company-dabbrev)
          ))
  (set-face-attribute
   'company-preview nil :foreground "black" :underline t)
  (set-face-attribute
   'company-preview-common nil :inherit 'company-preview)
  (set-face-attribute
   'company-tooltip nil :background "lightgray" :foreground "black")
  (set-face-attribute
   'company-tooltip-selection nil :background "steelblue" :foreground "white")
  (set-face-attribute
   'company-tooltip-common nil :foreground "darkgreen" :weight 'bold)
  (set-face-attribute
   'company-tooltip-common-selection nil :foreground "black" :weight 'bold)
  )

(use-package company-lsp
  :straight t 
  :requires company
  :config
  (push 'company-lsp company-backends)

  ;; Disable client-side cache because the LSP server does a better job.
  (setq company-transformers nil
        company-lsp-async t
        company-lsp-cache-candidates nil))


(use-package company-statistics
  :straight t
  :after company
  :init (company-statistics-mode)
  )

(use-package company-web
  :straight t)

(use-package company-try-hard
  :straight t
  :after company
  :bind
  (("C-<tab>" . company-try-hard)
   :map company-active-map
   ("C-<tab>" . company-try-hard)))

(use-package company-quickhelp
  :straight t
  :after company
  :init (company-quickhelp-mode 1)
  :config (eval-after-load 'company
            '(define-key company-active-map (kbd "C-c h") #'company-quickhelp-manual-begin))
  (company-quickhelp-mode))

;;emojis
(use-package company-emoji
  :straight t
  :after company
  :hook
  ((markdown-mode . company-mode)
   (git-commit-mode . company-mode))
  :config
  (add-to-list 'company-backends 'company-emoji))

(use-package emojify
  :straight t
  :hook
  ((markdown-mode . emojify-mode)
   (git-commit-mode . emojify-mode)
   (magit-status-mode . emojify-mode)
   (magit-log-mode . emojify-mode)))

;;helm
(use-package helm
  :straight t
  :demand t
  ;; :requires helm-core
  :preface (use-package helm-config :ensure nil)
  :defines
  helm-ff-ido-style-backspace
  helm-ff-file-name-history-use-recentf
  helm-ff-newfile-prompt-p
  helm-ff-skip-boring-files
  helm-ff-auto-update-initial-value
  helm-ff--auto-update-state
  :bind
  ("M-x" . helm-M-x)
  ("C-s" . swiper)
  ("M-y" . helm-show-kill-ring) ;; replace yank-pop
  ("C-h SPC" . helm-all-mark-rings)
  ("C-x C-f" . helm-find-files)
  ("C-x C-b" . helm-buffers-list)
  ("C-x b" . helm-mini)
  ("M-/" . helm-dabbrev)
  :config
  (setq helm-move-to-line-cycle-in-source t
        helm-ff-file-name-history-use-recentf t
        helm-ff-newfile-prompt-p nil
        helm-ff-skip-boring-files t
        helm-ff-ido-style-backspace 'always
        helm-ff-auto-update-initial-value t
        helm-ff--auto-update-state t
        helm-scroll-amount 4 ; scroll 4 lines other window using M-<next>/M-<prior>
        helm-quick-update t ; do not display invisible candidates
        helm-idle-delay 0.01 ; be idle for this many seconds, before updating in delayed sources.
        helm-input-idle-delay 0.01 ; be idle for this many seconds, before updating candidate buffer
        helm-split-window-default-side 'other ;; open helm buffer in another window
        helm-split-window-in-side-p t ;; open helm buffer inside curRent window, not occupy whole other window
        helm-candidate-number-limit 200 ; limit the number of displayed canidates
        helm-move-to-line-cycle-in-source nil ; move to end or beginning of source when reaching top or bottom of source.
        ;; helm-command
        helm-M-x-requires-pattern 0     ; show all candidates when set to 0
        ))

(use-package helm-config
  :ensure nil
  :requires helm                
  :defines
  shell-mode-map
  :functions my-add-to-helm-boring-file-regexp-list
  :init
  (progn
    (helm-mode 1)
    (helm-adaptive-mode 1)
    ;; (helm-push-mark-mode 1)
    (custom-set-variables
     '(helm-command-prefix-key "C-c h")))
  :config
  (define-key shell-mode-map (kbd "C-c C-l") 'helm-comint-input-ring)
  (define-key minibuffer-local-map (kbd "C-c C-l") 'helm-minibuffer-history)
  (define-key helm-command-map (kbd "o" ) 'helm-occur)
  (define-key helm-command-map (kbd "M-s o") nil)
  (setq helm-boring-buffer-regexp-list (list (rx "*magit-") (rx "*helm") (rx "*//epc")))
  :diminish helm-mode
  )

(use-package helm-rg-occur
  :straight (:host github :repo "leuven65/helm-rg-occur")
  :after helm
  :custom
  (helm-rg-occur-file-size (* 1 1024 1024)) ; 2M
  )

(use-package helm-rg
  :straight t
  :bind ("C-º" . helm-rg))

(use-package helm-files
  :ensure nil
  :bind ([f7] . helm-browse-project))

(use-package helm-swoop
  :straight t)

(use-package swiper-helm
  :straight t)

;;programming
(use-package flyspell
  :straight t
  :config
  (when (eq system-type 'windows-nt)
    (add-to-list 'exec-path "C:/Program Files (x86)/Aspell/bin/"))
  (setq ispell-program-name "aspell" ; use aspell instead of ispell
        ispell-extra-args '("--sug-mode=ultra"))
  :hook ((text-mode . flyspell-mode)
         (prog-mode . flyspell-prog-mode)))

(use-package async
  :straight t
  :config
  (async-bytecomp-package-mode 1)
  (dired-async-mode 1)
  (autoload 'dired-async-mode "dired-async.el" nil t)
  )

;; (use-package flycheck
;;   :straight t
;;   :defer 2
;;   :diminish
;;   :config (setq flycheck-check-syntax-automatically '(mode-enabled save))
;;   :init (global-flycheck-mode)
;;   :hook (after-init-hook . global-flycheck-mode)
;;   (prog-mode . flycheck-mode)
;;   :custom
;;   (flycheck-display-errors-delay .3)
;;   (flycheck-stylelintrc "~/.stylelintrc.json"))

;; Other pkgs
;; (use-package flycheck-tip
;;   :straight t
;;   :commands flycheck-tip-cycle
;;   :after flycheck
;;   :bind (:map flycheck-mode-map
;;               ("C-c C-n" . flycheck-tip-cycle)))


(use-package rainbow-delimiters
  :straight t
  :hook
  (prog-mode . rainbow-delimiters-mode)
  (yaml-mode . rainbow-delimiters-mode)
  :diminish rainbow-delimiters-mode
  :commands rainbow-delimiters-mode
  )

(use-package rainbow-identifiers
  :straight t
  :hook
  (prog-mode . rainbow-identifiers-mode)
  (yaml-mode . rainbow-identifiers-mode)
  :diminish rainbow-identifiers-mode
  :commands rainbow-identifiers-mode
  )

(use-package rainbow-mode
  :straight t
  
  :mode "\\.css\\'"
  :hook (prog-mode-hook . rainbow-mode)
  :diminish rainbow-mode
  :commands rainbow-mode
  )

(use-package magit
  :straight t
  :config
  (magit-auto-revert-mode 1)
  (setq
   magit-auto-revert-immediately t)
  :bind (("C-x g" . magit-status)
         ("C-x M-g" . magit-dispatch-popup))
  )
(use-package libgit
  :straight t)

(use-package magit-delta
  :straight t
  :after magit
  :hook (magit-mode . magit-delta-mode)
  :config
  (setq
   magit-delta-default-dark-theme "Monokai Extended"
   magit-delta-default-light-theme "Github"
   magit-delta-hide-plus-minus-markers nil)
  )

;; (use-package magit-libgit
;;   :straight t
;;   :after (magit libgit))

(use-package git-gutter
  :straight t
  :diminish git-gutter-mode
  :commands git-gutter-mode
  :init
  ;; always a column reserved, but no flickering
  (setq git-gutter:unchanged-sign ""))

(use-package git-timemachine
  ;; :straight t
  :commands git-timemachine
  :init (setq git-timemachine-abbreviation-length 4))

(use-package rg
  :straight rg
  :commands rg
  :ensure-system-package
  (rg . ripgrep))

;; (use-package ag
;;   :straight t
;;   :commands ag
;;   :ensure-system-package ag
;;   :init
;;   (setq
;;    ag-reuse-window t
;;    ag-highlight-search t)
;;   :config
;;   (add-hook 'ag-search-finished-hook
;;             (lambda () (pop-to-buffer next-error-last-buffer))))

(use-package aggressive-indent
  :straight t
  :hook
  (prog-mode-hook . aggresive-indent-mode)
  :custom
  (aggressive-indent-comments-too t)
  :config
  (add-to-list 'aggressive-indent-protected-commands 'comment-dwim))

;; (use-package elisp-autofmt
;;   :commands (elisp-autofmt-save-hook-for-this-buffer)
;;   :hook (emacs-lisp-mode . elisp-autofmt-save-hook-for-this-buffer)

;;   :straight
;;   (elisp-autofmt
;;     :type git
;;     :host gitlab
;;     :files (:defaults "elisp-autofmt")
;;     :repo "ideasman42/emacs-elisp-autofmt"))

(use-package smartparens
  :diminish smartparens-mode
  :commands
  smartparens-mode
  sp-local-pair
  :config
  (require 'smartparens-config)
  :bind
  (:map
   smartparens-mode-map
   ("M-<delete>" . sp-unwrap-sexp)
   ("s-{" . sp-rewrap-sexp)))

(use-package yasnippet
  :straight t
  :init (yas-global-mode 1)
  :commands yas-minor-mode
  :hook (prog-mode . yas-minor-mode)
  :bind ("s-<tab>" . yas-expand)
  :config
  (add-hook 'yas-minor-mode-hook (lambda () (yas-activate-extra-mode 'fundamental-mode)))
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (yas-reload-all nil t)
  :diminish yas-minor-mode
  )

;;sudo apt install elpa-yasnippet-snippets
(use-package yasnippet-snippets
  :straight t)

(use-package yatemplate
  :straight t
  :defer 2 ;; WORKAROUND https://github.com/mineo/yatemplate/issues/3
  :init
  (setq auto-insert-alist nil)
  (setq-default yatemplate-license "http://www.gnu.org/licenses/lgpl-3.0.en.html")
  :config
  (auto-insert-mode 1)
  (yatemplate-fill-alist))

(use-package json-mode
  :straight t
  )

(use-package dockerfile-mode
  :straight t
  :mode ((rx "Dockerfile" eos) . dockerfile-mode)
  )

(use-package yaml-mode
  :straight t
  :config
  (add-to-list 'auto-mode-alist '("\\.yml" . yaml-mode))
  )

(use-package sqlformat
  :straight t)

(use-package web-mode
  :straight t
  :config
  (add-to-list 'auto-mode-alist '("\\.erb$" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.hbs$" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tmpl$" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html$" . web-mode))
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-code-indent-offset 2))

(use-package web-beautify
  :straight t)

(use-package tidy
  :straight t)

(use-package direnv
  :straight t
  :config
  (direnv-mode))

(use-package lsp-bridge
  :straight '(lsp-bridge :type git :host github :repo "manateelazycat/lsp-bridge"
			 :files (:defaults "*.el" "*.py" "acm" "core" "langserver" "multiserver" "resources")
			 :build (:not compile))
  ;; :init
  ;; (global-lsp-bridge-mode)
  :config
  (setq lsp-pylsp-plugins-rope-autoimport-enabled t)
  )

(use-package lsp-mode
  :straight t
  :commands lsp lsp-deferred
  :hook (lsp-after-open . lsp-enable-imenu)
  (lsp-deferred . lsp-enable-which-key-integration)
  (lsp-after-open . (lambda ()
                      (setq-local company-minimum-prefix-length 1
                                  company-idle-delay 0.0) ;; default is 0.2
                      ))
  (prog-mode . lsp)
  (python-mode . lsp)
  (lsp . company-mode)
  :bind (:map lsp-mode-map
              ("C-c C-d" . lsp-describe-thing-at-point))
  :config
  (setq gc-cons-threshold 100000000
	read-process-output-max (* 1024 1024)
	lsp-prefer-flymake nil
	lsp-auto-guess-root t ; Detect project root
        lsp-keep-workspace-alive nil ; Auto-kill LSP server
        lsp-prefer-capf t
        lsp-enable-indentation nil
        lsp-enable-symbol-highlighting nil
        lsp-enable-on-type-formatting nil
	lsp-idle-delay 0.500)
  :init
  (setq lsp-auto-guess-root t       ; Detect project root
        lsp-keymap-prefix "C-c l"
        lsp-log-io t
        lsp-enable-indentation t
        lsp-enable-imenu t
        lsp-file-watch-threshold 500
        lsp-prefer-flymake t
	)
  (defun lsp-on-save-operation ()
    (when (or (boundp 'lsp-mode)
             (bound-p 'lsp-deferred))
      (lsp-organize-imports)
      (lsp-format-buffer)))
  )


(use-package lsp-jedi
  :straight t
  :config
  (with-eval-after-load "lsp-mode"
    (add-to-list 'lsp-disabled-clients 'pyls)
    (add-to-list 'lsp-enabled-clients 'jedi)))

(use-package treemacs
  :straight t
  :bind ("<f8>" . 'treemacs)
  :hook (treemacs-mode . (lambda () (setq mode-line-format nil)))
  :config
  (progn
    (doom-themes-treemacs-config)
    (setq 
     treemacs-change-root-without-asking t
     treemacs-collapse-dirs              (if (executable-find "python") 3 0)
     treemacs-file-event-delay           5000
     treemacs-follow-after-init          t
     treemacs-follow-recenter-distance   0.1
     treemacs-goto-tag-strategy          'refetch-index
     treemacs-indentation                2
     treemacs-indentation-string         " "
     treemacs-max-git-entries            5000
     treemacs-is-never-other-window      nil
     treemacs-no-png-images              nil 
     treemacs-recenter-after-file-follow t
     treemacs-recenter-after-tag-follow  nil
     treemacs-show-hidden-files          nil 
     treemacs-silent-filewatch           t
     treemacs-silent-refresh             t
     treemacs-sorting                    'alphabetic-desc
     treemacs-tag-follow-cleanup         t
     treemacs-tag-follow-delay           1.5
     treemacs-persist-file              (f-join user-emacs-directory ".cache" "treemacs-persist") 
     treemacs-width                      30))
  )

(use-package treemacs-projectile
  :straight t
  :defer t
  :config
  (setq treemacs-header-function #'treemacs-projectile-create-header))

(use-package treemacs-magit
  :straight t)

(use-package treemacs-all-the-icons
  :straight t)

;; fix lsp-treemacs for zoom usage
(defun fix-lsp-treemacs (&rest _)
  (with-selected-window (get-buffer-window "*LSP Symbols List*")
    (setq window-size-fixed t)
    (let ((width 30))
      (window-resize (selected-window) (- width (window-total-width)) t t))))

;; advice after fix
(advice-add 'lsp-treemacs-render :after 'fix-lsp-treemacs)

(use-package lsp-treemacs
  :straight t
  :after lsp
  :config
  (lsp-treemacs-sync-mode 1)
  )



;; debug mode
(use-package dap-mode
  :straight t
  )

(use-package helm-lsp
  :straight t
  :commands helm-lsp-workspace-symbol)

(use-package lsp-ui
  :straight t
  :commands lsp-ui-mode
  :requires lsp-mode flymake
  :hook (lsp-deferred . lsp-ui-mode)
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  (setq lsp-ui-sideline-enable t
        lsp-ui-sideline-update-mode 'line
        lsp-ui-sideline-show-code-actions t
        lsp-ui-sideline-show-diagnostics t
        lsp-ui-sideline-ignore-duplicate t
        lsp-ui-sideline-show-hover t
	lsp-ui-sideline-delay 1
        lsp-eldoc-enable-hover nil ; Disable eldoc displays in minibuffer
        lsp-ui-doc-enable t
	;; lsp-ui-flymake-enable t
        ;; lsp-ui-flymake-live-reporting t
	;; lsp-ui-flymake-list-position 'right
        lsp-ui-doc-position 'at-point
        lsp-ui-doc-include-signature t
        lsp-ui-imenu-enable t
	lsp-ui-imenu-auto-refresh t
	lsp-ui-peek-enable t
        lsp-ui-peek-show-directory t
        )
  (add-to-list 'lsp-ui-doc-frame-parameters '(right-fringe . 8))
  ;; `C-g'to close doc
  (advice-add #'keyboard-quit :before #'lsp-ui-doc-hide)
  ;; Reset `lsp-ui-doc-background' after loading theme
  (add-hook 'after-load-theme-hook
            (lambda ()
              (setq lsp-ui-doc-border (face-foreground 'default))
              (set-face-background 'lsp-ui-doc-background
                                   (face-background 'tooltip))))
  ;; WORKAROUND Hide mode-line of the lsp-ui-imenu buffer
  ;; @see https://github.com/emacs-lsp/lsp-ui/issues/243
  ;; (defadvice lsp-ui-imenu (after hide-lsp-ui-imenu-mode-line activate)
  ;;   (setq mode-line-format nil))
  )


(use-package js2-mode
  :straight t
  :defer t
  :mode "\\.js$"
  :config
  (add-hook 'js2-mode-hook (lambda () (setq mode-name "js2")))
  (setf js2-skip-preprocessor-directives t)
  (setq-default js2-additional-externs
                '("$" "unsafeWindow" "localStorage" "jQuery"
                  "setTimeout" "setInterval" "location" "skewer"
                  "console" "phantom")))


;; (use-package company
;;   :straight t
;;   :config
;;   (setq company-idle-delay 0)
;;   (setq company-minimum-prefix-length 1))


(use-package markdown-mode
  :straight t
  :mode "\\.md\\'"
  :hook
  (markdown-mode . flyspell-mode))

(use-package dashboard
  :straight t
  :diminish dashboard-mode
  :config
  ;; (setq dashboard-banner-logo-title "your custom text")
  ;; (setq dashboard-startup-banner "/path/to/image")
  (setq dashboard-items '((recents  . 10)
			  (projects . 5)
                          ;;(bookmarks . 10)
			  ))
  (dashboard-setup-startup-hook)
  :defer nil
  )

(use-package restclient
  :straight t)

;; sublimity
;; (use-package sublimity
;;   :disabled t
;;   :straight t
;;   :config
;;   (use-package sublimity-scroll
;;     :ensure nil)
;;   ;; (setq sublimity-scroll-weight 5
;;   ;;       sublimity-scroll-drift-length 100)
;;   ;; (setq sublimity-scroll-vertical-frame-delay 0.01)
;;   (sublimity-mode 1))

                                        ;  (sublimity-map-set-delay 3))
(use-package minimap
  :straight t
  :bind ("C-<f2>". minimap-mode)
  )

(use-package perfect-margin
  :straight t
  :custom
  (perfect-margin-visible-width 128)
  :diminish perfect-margin-mode
  :config
  ;; enable perfect-mode
  (perfect-margin-mode t)
  ;; add additinal bding on margin area
  ;; (dolist (margin '("<left-margin> " "<right-margin> "))
  ;;   (global-set-key (kbd (concat margin "<mouse-1>")) 'ignore)
  ;;   (global-set-key (kbd (concat margin "<mouse-3>")) 'ignore)
  ;;   (dolist (multiple '("" "double-" "triple-"))
  ;;     (global-set-key (kbd (concat margin "<" multiple "wheel-up>")) 'mwheel-scroll)
  ;;     (global-set-key (kbd (concat margin "<" <multiple "wheel-down>")) 'mwheel-scroll)))
  )

(provide 'init)
;;; init.el ends here
(put 'scroll-left 'disabled nil)
