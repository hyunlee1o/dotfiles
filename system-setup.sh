#!/bin/bash

# bool function to test if the user is root or not (POSIX only)
is_user_root () { [ "$(id -u)" -eq 0 ]; }

if is_user_root; then
    echo 'You are the almighty root! But better call this without sudo'
    exit 1
fi

refreshPermissions () {
    local pid="${1}"

    while kill -0 "${pid}" 2> /dev/null; do
        sudo -v
        sleep 10
    done
}

refreshPermissions "$$" &

download_dir="/tmp"
persistent_download_dir=$HOME/Descargas
padding="#########    "

mkdir -p $HOME/.local/bin
mkdir -p $persistent_download_dir

# add contrib non-free to sources.list
# echo "add contrib non-free to repo like:\n     deb http://deb.debian.org/debian/ bullseye main contrib non-free"
echo $padding ADDING EXTRA REPOS $padding
sudo sed -i -e "s/ main[[:space:]]*\$/ main contrib non-free-firmware/" /etc/apt/sources.list
sudo apt update

echo $padding INSTALLING CORE DEPENDENCIES $padding
sudo apt install -m -y

for i in feh vlc flameshot strawberry vlc mpv smplayer meld gnome-disk-utility gitk gimp qbittorrent udiskie lutris obs-studio  torbrowser-launcher\
	     fish scrot curl git bpytop p7zip software-properties-common gdebi rofi fish fzf\
	     python3-pip python3-dev redshift python3-venv python-is-python3 black \
	     eza bat python3-launchpadlib jq ffmpeg adb ripgrep direnv pipx \
	 apt-transport-https ca-certificates curl gnupg lsb-release i3 broadcom-sta-dkms linux-headers-amd64 isenkram-cli; # Debian stuff WIFI
do sudo apt install -y $i;
done;

sudo isenkram-autoinstall-firmware

sudo add-apt-repository ppa:nicotine-team/stable
sudo apt update
# edit the sources.list from nicotine before sudo apt install nicotine
sudo apt install nicotine

# Add docker dependencies
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

LATEST_DOCKER_DIST=`curl -Ls https://download.docker.com/linux/debian/dists/|grep "href.*"| head -n 2| tail +2 | grep -zoP '<a href="\K[^a/"]+'`
# Check for the latest docker dist
if [ -z $LATEST_DOCKER_DIST ];
then
    DOCKER_DIST = $LATEST_DOCKER_DIST
else
    DOCKER_DIST =  $(lsb_release -cs)
fi
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $DOCKER_DIST stable" | sudo tee /etc/apt/sources.list.d/docker-testing.list > /dev/null;

echo $padding INSTALLING DOCKER $padding
# install docker and compose
sudo apt remove docker docker-engine docker.io containerd runc
sudo apt install -y docker-compose docker-ce docker-ce-cli containerd.io 

# ADD indicator sound switcher repo
sudo apt-add-repository -y ppa:yktooo/ppa
sudo apt update

echo $padding INSTALLING VIDEO DRIVERS $padding
# https://wiki.debian.org/AtiHowTo#AMD.2FATI_Drivers_.28amdgpu.2C_radeon.2C_r128.2C_mach64.29
#steam dependencies and amd dependencies
sudo apt install -y  firmware-amd-graphics libgl1-mesa-dri libglx-mesa0 mesa-vulkan-drivers xserver-xorg-video-all radeontop
curl -Ls -O https://cdn.fastly.steamstatic.com/client/installer/steam.deb --output_dir $download_dir && sudo apt install $download_dir/steam.deb 
# sudo apt install -y libgl1-mesa-dri:i386 libgl1-mesa-glx:i386


echo $padding INSTALLING PYTHON DEPENDENCIES $padding
# tool for monitoring AMD card usage
pipx install amdgpu-fan 
pipx python-lsp-server['all']
pipx powerline-shell 
python3 -c "$(wget -q -O - https://raw.githubusercontent.com/wakatime/vim-wakatime/master/scripts/install_cli.py)" "~/.wakatime/wakatime-cli"

echo $padding UPGRADE EVERYTHING $padding
sudo apt full-upgrade -y

echo $padding INSTALL FISH CONFIGURATION $padding
# change default shell to fish
sudo chsh -s `command -v fish`
mkdir -p ~/.config/fish/

# Add direnv support in fish
grep -qxF 'direnv hook fish | source' ~/.config/fish/config.fish || echo 'direnv hook fish | source' >> ~/.config/fish/config.fish
# Add .local/bin to path 
grep -qxF '/home/jorge/.local/bin' ~/.config/fish/config.fish || /usr/bin/fish -c 'fish_add_path -a /home/jorge/.local/bin'
# remove bad prompt
grep -qxF 'set -x VIRTUAL_ENV_DISABLE_PROMPT 1' ~/.config/fish/config.fish || echo 'set -x VIRTUAL_ENV_DISABLE_PROMPT 1' >> ~/.config/fish/config.fish

# Check if fish_prompt has already been overriden (to powerline)
grep -qxF 'function fish_prompt' ~/.config/fish/config.fish || echo """function fish_prompt
    if test $TERM = \"dumb\"
	echo \"\\$ \"
    else
	powerline-shell --shell bare $status
    end
end""" >> ~/.config/fish/config.fish

# install fisher
fish -c "curl -sfL https://git.io/fisher | source && fisher install jorgebucaran/fisher"
# fisher plugins
fish -c "fisher install danhper/fish-ssh-agent gazorby/fish-abbreviation-tips install decors/fish-colored-man install hyunlee1o/fish-eza"
fish -c "fisher update"

echo $padding INSTALL POWERLINE CONFIGURATION $padding
# powerline-shell https://github.com/b-ryan/powerline-shell
mkdir -p ~/.config/powerline-shell && \
    powerline-shell --generate-config > ~/.config/powerline-shell/config.json

get_latest_release() {
    curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
	grep '"tag_name":' |                                            # Get tag line
	sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}

echo $padding INSTALL INDICATOR SOUND SWITCHER $padding
# sudo apt install indicator-sound-switcher
sudo apt install -y python3-gi gir1.2-gtk-3.0 gir1.2-ayatanaappindicator3-0.1 gir1.2-keybinder-3.0
fix_indicator_sound_switcher() {
    cd $download_dir
    sudo apt install zstd binutils
    sudo apt download indicator-sound-switcher
    ar x indicator-sound-switcher_*_all.deb
    zstd -d < control.tar.zst | xz > control.tar.xz
    zstd -d < data.tar.zst | xz > data.tar.xz
    ar -m -c -a sdsd indicator-sound-switcher_repacked.deb debian-binary control.tar.xz data.tar.xz
    rm debian-binary control.tar.xz data.tar.xz control.tar.zst data.tar.zst
    sudo apt install -y $download_dir/indicator-sound-switcher_repacked.deb
}
fix_indicator_sound_switcher

# download git-delta
echo $padding INSTALL GIT DELTA https://github.com/dandavison/delta/ $padding
# https://github.com/dandavison/delta/releases/latest
rm $download_dir/git-delta_*.deb
curl -Ls https://api.github.com/repos/dandavison/delta/releases/latest \
    | grep "browser_download_url.*amd64.deb" \
    | cut -d : -f 2,3 \
    | tr -d \" \
    | xargs -n1 curl --output-dir $download_dir -L -O $1
sudo apt install -y $download_dir/git-delta_*.deb

# make sure the correct ripgrep is downloaded (debian default is bad)
echo $padding INSTALL RIPGREP https://api.github.com/repos/BurntSushi/ripgrep/ $padding
# rm $download_dir/ripgrep_*amd64.deb
# curl -Ls https://api.github.com/repos/BurntSushi/ripgrep/releases/latest \
    #    | grep "browser_download_url.*amd64.deb" \
    #    | cut -d : -f 2,3 \
    #    | tr -d \" \
    #    | xargs -n1 curl --output-dir $download_dir -L -O $1
#sudo apt install -y $download_dir/ripgrep_*amd64.deb

echo $padding INSTALL FD https://github.com/sharkdp/fd $padding
#download fd
rm $download_dir/fd_*amd64.deb
curl -Ls https://github.com/sharkdp/fd/releases/latest \
    | grep "href.*fd_.*amd64.deb" \
    | grep -zoP '<a href="\K[^"]+' \
    | xargs -n1 curl --output-dir $download_dir -L -O "https://github.com"$(</dev/stdin)
sudo apt install -y $download_dir/fd_*amd64.deb

echo $padding INSTALL WAKATIME https://github.com/sharkdp/fd $padding
# INSTALL WAKATIME
curl -Ls https://github.com/wakatime/wakatime-cli/releases/latest \
    | grep "href.*linux-amd64" \
    | grep -zoP '<a href="\K[^"]+' \
    | xargs -n1 curl --output-dir $download_dir -L -O "https://github.com"$(</dev/stdin)   
unzip -o $download_dir/wakatime-cli-linux-amd64.zip -d $download_dir
chmod +x $download_dir/wakatime-cli-linux-amd64
sudo cp $download_dir/wakatime-cli-linux-amd64 /usr/local/bin/wakatime

echo $padding INSTALL NoiseTorch https://github.com/noisetorch/NoiseTorch $padding
curl -s https://api.github.com/repos/noisetorch/NoiseTorch/releases/latest |
    grep "browser_download_url.*tgz\"$" |
    cut -d : -f 2,3 |
    xargs -n1 curl --output-dir $download_dir -L -O
tar -C $HOME -h -xzf $download_dir/NoiseTorch_x64_v0.12.2.tgz
gtk-update-icon-cache
sudo setcap 'CAP_SYS_RESOURCE=+ep' $HOME/.local/bin/noisetorch

# install latest firefox
if [ ! -d "/opt/firefox/" ]; then
    echo $padding INSTALL FIREFOX $padding
    curl -o $download_dir/firefox-latest.tar.bz2 -L "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=es-ES"
    sudo tar xjf $download_dir/firefox-latest.tar.bz2 -C /opt/
    sudo chown -R $USER:$USER /opt/firefox
    rm $download_dir/firefox-latest.tar.bz2
    sudo ln -sfn /opt/firefox/firefox /usr/bin/firefox 
    echo "Firefox symlink does not exist. Generate"
    sudo cp /usr/share/applications/firefox-esr.desktop /usr/share/applications/firefox.desktop
    # replace ESR with nothing and change the exec path
    sudo sed -i 's/ESR//; s/Exec=[^ ]*/Exec=\/opt\/firefox\/firefox/'  /usr/share/applications/firefox.desktop
fi

echo $padding INSTALL EMACS $padding
#emacs
sudo apt build-dep emacs
sudo apt install -y libgccjit-12-dev libjansson-dev

if [ ! -d $persistent_download_dir/emacs ]; then
    git clone https://github.com/emacs-mirror/emacs
fi
cd $persistent_download_dir/emacs
git pull
./autogen.sh
make bootstrap -j
sudo make install

echo $padding DOWNLOAD DOTFILES :\) $padding
#dotfiles
cd $persistent_download_dir/
if [ ! -d $persistent_download_dir/dotfiles ]; then
    git clone git@bitbucket.org:hyunlee1o/dotfiles.git
fi
cd $persistent_download_dir/dotfiles
git pull
# cp Descargas/dotfiles/.i3/config ~/.config/i3/config

cd $persistent_download_dir
if [ ! -d $persistent_download_dir/linux-firmware ]; then
    git clone https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git
    cd $persistent_download_dir/dotfiles
else
    cd $persistent_download_dir/dotfiles
    git pull
fi
# sudo cp -vapu * /lib/firmware/
sudo make install

echo $padding DOWNLOAD ICONS IN TERMINAL :\) $padding
# icons in terminal
cd $download_dir/
if [ ! -d $download_dir/icons-in-terminal ]; then
    git clone https://github.com/sebastiencs/icons-in-terminal.git;
    cd icons-in-terminal
else
    cd $download_dir/icons-in-terminal
    git pull
fi    
bash install.sh

grep -qxF 'source ~/.local/share/icons-in-terminal/icons.fish' ~/.config/fish/config.fish || echo 'source ~/.local/share/icons-in-terminal/icons.fish' >> ~/.config/fish/config.fish
'set -x PATH /home/jorge/.local/bin $PATH'
'set -x VIRTUAL_ENV_DISABLE_PROMPT 1'


# install hack font
curl -fsSL https://raw.githubusercontent.com/getnf/getnf/main/install.sh | bash
getnf -i Hack
# arreglar el brillo en portatil
#cd $download_dir/
#git clone https://gitlab.com/wavexx/acpilight.git
#cd acpilight
#sudo make install
sudo apt install debian-archive-keyring
