function less --wraps less --description 'Format and display with less'
    set -l less_path (which less)
    set -l pygmentize_path (which pygmentize)
    if test $status -ne 0
	echo "pygments is not installed"
    end
    $pygmentize_path -g $argv[-1] | $less_path -R $argv[1..-2]
end
